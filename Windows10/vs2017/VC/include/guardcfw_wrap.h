#ifndef _GUARDCFW_WRAP_H
#define _GUARDCFW_WRAP_H 1

#ifndef NT_INCLUDED
#include <windows.h>
#endif


// extra space after malloced memory
// to prevent heap metadata corruption from unaligned writes
    
#if defined(_M_X64)
// since heap metadata on amd64 is only 8-bytes long, we add an
// extra 8 bytes when SLOT_SIZE is 16 to make sure allocations have a 
// a non-writable slot between them.
# if 0 // SLOT_SIZE == 16 // SLOT_SIZE is undefined in this context
# define _GUARD_HEAP_ALLOC_PAD_SIZE 8
# else
# define _GUARD_HEAP_ALLOC_PAD_SIZE 0
# endif

#elif defined(_M_IX86)
#define _GUARD_HEAP_ALLOC_PAD_SIZE 0

#elif defined(_M_ARM)
#define _GUARD_HEAP_ALLOC_PAD_SIZE 0

#elif defined(_M_ARM64)
#define _GUARD_HEAP_ALLOC_PAD_SIZE 0

#else
#error unknown architecture for _GUARD_HEAP_ALLOC_PAD_SIZE
#endif


#ifdef  __cplusplus
extern "C" {
#endif

/* Functions for implementing wrappers.  The unsafe variety should
 * only be used for threads updating their own private stack, or for
 * whole-pages where the bitmap words cannot have an access
 * conflict. */
void __cdecl GuardCheckWrite(const void *_p, size_t _s);
void __cdecl GuardGrantWriteUnsafe(const void *_p, size_t _s);
void __cdecl GuardRevokeWriteUnsafe(const void *_p, size_t _s);
void __cdecl GuardGrantWriteLocked(const void *_p, size_t _s);
void __cdecl GuardRevokeWriteLocked(const void *_p, size_t _s);
void __cdecl GuardCheckAndRevokeWrite(const void *p, size_t s);
void __cdecl GuardGrantWriteCheckGuards(const void *_p, size_t _s);
size_t __cdecl GuardRevokeWriteScan(const void *_p);
void __cdecl GuardGrantWriteSlots(const void *_p, size_t _s);
int __cdecl GuardGrantWriteIfNotWritable(const void *_p, size_t _s);
void __cdecl GuardCheckICall(const void *_p);
void __cdecl GuardGrantICall(const void *_p);
void __cdecl GuardRevokeICall(const void *_p);

// initialisation function
void __cdecl _guard_init(PVOID baseAddr);

// COM wrapping support
typedef void (__cdecl *PGuardComInitializer)();
#if defined(_UNKNOWN_H_) || defined(__IUnknown_INTERFACE_DEFINED__)
typedef void*  (__cdecl *PGuardComAllocator)(IUnknown* interf);
void __cdecl GuardAddComFactoryClient(REFIID iid, PGuardComAllocator alloc, int numMethods);
void __cdecl GuardAddComFactoryServer(REFIID iid, PGuardComAllocator alloc, int numMethods);
void* __cdecl GuardAllocateClientWrapper(REFIID iid, void** interf, void* outer, BOOL addRef);
void* __cdecl GuardAllocateServerWrapper(REFIID iid, void** interf, void* outer, BOOL addRef);

void __cdecl GuardAllocateServerWrapperArray(
    REFIID iid,
    _In_reads_(size) PVOID *interfIn,
    _Out_writes_all_(size) PVOID *interfOut,
    _Out_writes_all_(size) PVOID *wrappers,
    _In_ size_t size);

void __cdecl GuardAllocateClientWrapperArray(
    REFIID iid,
    _In_reads_(size) PVOID *interfIn,
    _Out_writes_all_(size) PVOID *interfOut,
    _Out_writes_all_(size) PVOID *wrappers,
    _In_ size_t size);

void __cdecl GuardReleaseWrapper(void* interf);
void __cdecl GuardReleaseWrapperArray(void** interf, size_t size);
void __cdecl GuardFillClientWrapperArray(REFIID iid, void** interfIn, size_t size); 
void __cdecl GuardFillServerWrapperArray(REFIID iid, void** interfIn, size_t size);

void* __cdecl GuardAllocateClientAggregatingWrapper(REFIID iid, void** interf);
void* __cdecl GuardAllocateServerAggregatingWrapper(REFIID iid, void** interf);
void __cdecl GuardReleaseAggregatingWrapper(void* interf);

// COM VARIANT wrapping support
typedef struct tagVARIANT VARIANT;
void* __cdecl GuardAllocateClientVariantWrapper(VARIANT* v, BOOL addRef);
void* __cdecl GuardAllocateServerVariantWrapper(VARIANT* v, BOOL addRef);

void __cdecl GuardAllocateServerVariantWrapperArray(
    _In_reads_(size) VARIANT* variantsIn,
    _Out_writes_all_(size) VARIANT *variantsOut,
    _Out_writes_all_(size) PVOID *wrappers,
    _In_ size_t size);

void __cdecl GuardAllocateClientVariantWrapperArray(
    _In_reads_(size) VARIANT* variantsIn,
    _Out_writes_all_(size) VARIANT *variantsOut,
    _Out_writes_all_(size) PVOID *wrappers,
    _In_ size_t size);

void __cdecl GuardFillClientVariantWrapperArray(VARIANT* interfIn, size_t size); 
void __cdecl GuardFillServerVariantWrapperArray(VARIANT* interfIn, size_t size);

// Support for structures containing COM objects
typedef struct tagSTGMEDIUM STGMEDIUM;
typedef struct _tagBINDINFO BINDINFO;
typedef struct DWRITE_GLYPH_RUN DWRITE_GLYPH_RUN;
typedef struct D2D1_LAYER_PARAMETERS D2D1_LAYER_PARAMETERS;

void* __cdecl GuardAllocateClienttagSTGMEDIUMWrapper(STGMEDIUM *v, BOOL addRef);
void* __cdecl GuardAllocateServertagSTGMEDIUMWrapper(STGMEDIUM *v, BOOL addRef);
void* __cdecl GuardAllocateClient_tagBINDINFOWrapper(BINDINFO *v, BOOL addRef);
void* __cdecl GuardAllocateServer_tagBINDINFOWrapper(BINDINFO *v, BOOL addRef);
void* __cdecl GuardAllocateClientDWRITE_GLYPH_RUNWrapper(DWRITE_GLYPH_RUN *v, BOOL addRef);
void* __cdecl GuardAllocateServerDWRITE_GLYPH_RUNWrapper(DWRITE_GLYPH_RUN *v, BOOL addRef);
void* __cdecl GuardAllocateClientD2D1_LAYER_PARAMETERSWrapper(D2D1_LAYER_PARAMETERS *v, BOOL addRef); 
void* __cdecl GuardAllocateServerD2D1_LAYER_PARAMETERSWrapper(D2D1_LAYER_PARAMETERS *v, BOOL addRef);

void *__cdecl _alloca(_In_ size_t _Size);
#pragma intrinsic(_alloca)

#endif // _UNKNOWN_H_ || __IUnknown_INTERFACE_DEFINED__

#ifdef  __cplusplus
}
#endif

// assumes __p is side effect free (which is true when it is an expression from a SAL annotation)
#define __STRLEN(__p) ((sizeof(*(__p)) == sizeof(char)) ? strlen((char*)(__p)) : wcslen((wchar_t*)(__p))*sizeof(wchar_t))

#ifdef  __cplusplus
#if defined(_UNKNOWN_H_) || defined(__IUnknown_INTERFACE_DEFINED__)
// special wrappers for some standard COM interfaces

#if _MSC_VER >= 1700
#define GUARDIGNORE __declspec(guard(ignore))
#else
#define GUARDIGNORE
#endif

struct _guardComServer_IUnknown;

// Wrappers in interfaces derived from _guardComClient_IUnknown and _guardComServer_IUnknown cannot add new data members. 
// Any additional data they need to store in the wrapper must be stored in the context variable that points to something
// derived from GuardComWrapperContext. The context is delete when the wrapper is delete, i.e., when its reference count 
// goes to zero.
struct GuardComWrapperContext {
    GuardComWrapperContext() {};
    virtual ~GuardComWrapperContext() {}

    void* operator new(size_t size) { return HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, size); }
    void operator delete(void *p) { HeapFree(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, p); }
};

struct _guardComClient_IUnknown {
    IUnknown* real;                                             // pointer to object being wrapped
    ULONG count;                                                // local reference count
    int numMethods;                                             // number of methods in the wrapper
    _guardComClient_IUnknown* identityWrapper;                  // caches wrapper for IUnknown to implement COM identity requirements
    _guardComServer_IUnknown* outerWrapper;                     // pointer to wrapper for controlling IUnknown if the wrapper is part of an 
                                                                // binary aggregation cycle outer->this->inner->outerWrapper->outer
    GuardComWrapperContext* context;                            // pointer to wrapper specific context block if any
    _guardComClient_IUnknown* next;                             // to link wrappers in a list

    GUARDIGNORE _guardComClient_IUnknown(IUnknown* _real, int _numMethods) : 
        real(_real), count(1), numMethods(_numMethods), identityWrapper(NULL), outerWrapper(NULL), context(NULL), next(NULL) {}

    virtual HRESULT __stdcall QueryInterface(const IID & __p1,void ** __p2); 
    virtual ULONG __stdcall AddRef();  
    virtual ULONG __stdcall Release(); 

    void* operator new(size_t size) { return HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, size); }
    void operator delete(void *p) { HeapFree(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, p); }
};


// Server wrappers must have exactly the same layout as client wrappers (except that the vtable will be different)
struct _guardComServer_IUnknown {
    IUnknown* real;                                      // pointer to object being wrapped
    ULONG count;                                         // local reference count 
    int numMethods;                                      // number of methods in the wrapper
    _guardComServer_IUnknown* identityWrapper;           // caches wrapper for IUnknown to implement COM identity requirements
    _guardComClient_IUnknown* outerWrapper;              // pointer to wrapper for controlling IUnknown if the wrapper is part of an 
                                                         // binary aggregation cycle outer->this->inner->outerWrapper->outer
    GuardComWrapperContext* context;                     // pointer to wrapper specific context block if any
    _guardComClient_IUnknown* next;                      // to link wrappers in a list

    GUARDIGNORE _guardComServer_IUnknown(IUnknown* _real, int _numMethods) : 
        real(_real), count(1), numMethods(_numMethods), identityWrapper(NULL), outerWrapper(NULL), context(NULL), next(NULL) {}

    virtual HRESULT __stdcall QueryInterface(const IID & __p1,void ** __p2); 
    virtual ULONG __stdcall AddRef();  
    virtual ULONG __stdcall Release(); 

    void* operator new(size_t size) { return HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, size); }
    void operator delete(void *p) { HeapFree(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, p); }
};

#undef GUARDIGNORE

#endif // _UNKNOWN_H_ || __IUnknown_INTERFACE_DEFINED__
#endif //__cplusplus

// define our own assert to avoid CRT dependencies
#ifdef  NDEBUG
#define GuardAssert(_Expression)     ((void)0)
#else
#define GuardAssert(_Expression) ((!!(_Expression)) || (__debugbreak(), 0))
#endif  /* NDEBUG */


// Functions for detours related hooking

typedef LONG __cdecl GUARD_DETOURS_INIT_ROUTINE(VOID);
typedef GUARD_DETOURS_INIT_ROUTINE *PGUARD_DETOURS_INIT_ROUTINE;

#endif // _GUARDCFW_WRAP_H

