/***********************************************************************
* Microsoft Pogo
*
* Microsoft Confidential.  Copyright 1996-2000 Microsoft Corporation.
*
* Component:
*
* File: pogoapi.h
*
* File Comments:
*
*  Interface to the Pogosweep utility functions for storing/retrieving
*  a pointer to driver profile buffers (to ensure we don't reallocate
*  memory if the driver is loaded more than once.)
*
***********************************************************************/

bool PogoGetProfileBuffer(
    LPCSTR,
    size_t,
    PVOID* ppBuffer);

bool PogoSetProfileBuffer(
    LPCSTR,
    size_t,
    PVOID pBuffer,
    PVOID* ppOutBuffer
);

typedef struct tagSWEEPDRIVER_PARAM_PROFILE_SECTION
{
    WCHAR wzName[512];
    ULONG ulSize;
} SWEEPDRIVER_PARAM_PROFILE_SECTION;

typedef SWEEPDRIVER_PARAM_PROFILE_SECTION* PSWEEPDRIVER_PARAM_PROFILE_SECTION;
