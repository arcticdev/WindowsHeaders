/* Since we're force-included, we should stay
 * out of the way of MIDL and RC */
#if !defined(__midl) && !defined(RC_INVOKED)

/* User code and static CRT libs also need to link against the
 * runtime: */
#pragma comment(lib, "guardcfemu.lib")

#pragma guard(wrap, GetProcAddress)

#pragma guard(entry,main)
#pragma guard(entry,wmain)
#pragma guard(entry,WinMain)
#pragma guard(entry,LibMain)
#pragma guard(entry,DllMain)

#endif /* !defined(__midl) && !defined(RC_INVOKED) */
 
