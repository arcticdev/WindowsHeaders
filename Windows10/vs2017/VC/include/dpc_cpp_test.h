// ==++==
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//
// ==--==
// =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
//
// dpc_cpp_test.h
//
// cpp test harness header file.
//
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#pragma once

#include <concrt.h>
#include <ppl.h>
#include <amp.h>
#include <amprt.h>
#include <math.h>
#include <functional>

#ifdef  _DEBUG

using namespace Concurrency;

namespace Concurrency
{
    namespace Test 
    {
        namespace CPPTestHarness
        {
            /// <summary>
            /// A wrapper around cooperative event to enable _Spin_wait.
            /// </summary>
            class _Event_wrapper
            {
            public:

                _Event_wrapper() : _M_blocked_state(None)
                {
                }

                void _Set()
                {
                    if (_InterlockedExchange(&_M_blocked_state, Unblocked) == Blocked)
                    {
                        _M_event.set();
                    }
                }

                void _Wait(int _Spin_count = 8192)
                {
                    // Attempt to spin wait first
                    _Spin_wait(_Spin_count);

                    if ((_M_blocked_state != Unblocked) && (_InterlockedCompareExchange(&_M_blocked_state, Blocked, None) != Unblocked))
                    {
                        _M_event.wait(COOPERATIVE_TIMEOUT_INFINITE);
                    }
                }

                void _Reset()
                {
                    _M_event.reset();
                    _M_blocked_state = None;
                }

                void _Spin_wait(int _Spin_count)
                {
                    // Empirical evidence indicate that it is almost always beneficial to spin
                    // wait than block.

                    const int _Repeat = 128;
                    int _Retries = _Spin_count/_Repeat;

                    while ((_M_blocked_state != Unblocked) && (--_Retries > 0))
                    {
                        // Let other tasks run. When we are oversubscribed
                        // this would yield to other virtual processors once
                        // all the runnables are exhausted. Note that using
                        // SwitchToThread() here would be counter productive
                        // on UMS as it will only switch tasks within the 
                        // particular virtual processor on the scheduler.

                        _Context::_Yield();

                        int _Spin = 0;

                        while ((_M_blocked_state != Unblocked) && (++_Spin < _Repeat))
                        {
                            _YieldProcessor();
                        }
                    }

                    return;
                }

            private:

                // Enumeration of blocked states
                enum
                {
                    None,
                    Blocked,
                    Unblocked
                };

                // blocking state
                volatile long _M_blocked_state;

                // Underlying cooperative event
                Concurrency::event _M_event;
            };

            /// <summary>
            /// A ConcRT aware barrrier
            /// </summary>
            class _Barrier
            {
            public:

                /// <summary>
                /// Constructs a new barrier.
                /// </summary>
                /// <param name="count">Indicates the number of tasks.</param>
                _Barrier(size_t count)
                    : _M_numTasks(count), _M_num_waiters(0), _M_version(0), _M_event_index(0)
                {
                }

                void _Wait()
                {
                    // Capture the current event before incrementing the number of 
                    // waiters as the current event could be switched out as soon as the barrier
                    // is satisfied
                    _Event_wrapper * _Event_ptr = &_M_events[_M_event_index];

                    size_t _Num_waiters = _InterlockedIncrement(&_M_num_waiters);

                    if (_Num_waiters == _M_numTasks)
                    {
                        // Update the barrier version number
                        ++_M_version;

                        // prev = (curr + 1) % 2
                        int _Previous_index = (_M_event_index + 1) & 0x1;
                        _Event_wrapper * _Previous_event = &_M_events[_Previous_index];

                        // Make previous the event for the next barrier version
                        _Previous_event->_Reset();
                        _M_event_index = _Previous_index;
                        _M_num_waiters = 0;

                        // Signal the event for the current barrier version
                        _Event_ptr->_Set();
                    }
                    else
                    {
                        // Heuristic to determine the amount of spin. Currently, it only accounts
                        // for the cases where the number of tasks is greater than the number of 
                        // hardware threads

                        size_t _Remaining_tasks = _M_numTasks - _Num_waiters;
                        int _Spin_count = (_Remaining_tasks >= CurrentScheduler::Get()->GetNumberOfVirtualProcessors()) ? 0 : 8192;

                        _Event_ptr->_Wait(_Spin_count);
                    }
                }

            private:

                // We use 2 events to synchronize the barrier across
                // multiple iterations
                _Event_wrapper _M_events[2];

                // Index of the event for the current barrier version
                int _M_event_index;

                // Number of tasks that have reached and are waiting on the barrier
                volatile long _M_num_waiters;

                // The number of tasks participating in the barrier
                size_t _M_numTasks;

                // Barrier version
                size_t _M_version;

            };

            typedef unsigned int uint;

            struct three_dim_base 
            {
                unsigned int x;
                unsigned int y;
                unsigned int z;
            };

            struct group_id : public three_dim_base
            {
            };

            struct group_thread_id : public three_dim_base
            {
            };

            struct dispatch_thread_id : public three_dim_base
            {
            };

            // context for a group
            struct group_context_info
            {
                void ** ro_buffers;  // pointer to an array of readonly buffer pointers
                void ** rw_buffers;  // pointer to an array of read-write buffer pointers
                uint ** const_buffers; //pointer to constant buffer
                group_id gid; // group_id
                group_thread_id * gtids; // array of group_thread_id
                dispatch_thread_id * dtids; // array of dispatch_thread_id
            };

            // Mimic the StructuredBuffer
            template<typename _Elem_type>
            struct StructuredBuffer
            {
                // constructor
                StructuredBuffer(_In_ void * buf) 
                {
                    m_buffer = reinterpret_cast<_Elem_type *>(buf);
                }

                // [] operator
                const _Elem_type& operator[] (const uint index) const {
                    return m_buffer[index];
                }

            protected:
                _Elem_type * m_buffer;
            };

            // Mimic the RWByteAddressBuffer
            template<typename _Elem_type>
            struct RWStructuredBuffer : public StructuredBuffer<_Elem_type>
            {
                // constructor
                RWStructuredBuffer(_In_ void * buf) : StructuredBuffer(buf) {}

                _Elem_type& operator[] (const uint index) {
                    return m_buffer[index];
                }
            };

            // Mimic the ByteAddressBuffer
            
            struct ByteAddressBuffer
            {
                // constructor
                ByteAddressBuffer(_In_ void * buf) 
                {
                    m_buffer = reinterpret_cast<uint *>(buf);
                }

                // Load method
                uint Load(const uint index) const {
                    return m_buffer[index/4];
                }

            protected:
                uint * m_buffer;
            };

            // Mimic the RWByteAddressBuffer
            struct RWByteAddressBuffer : public ByteAddressBuffer
            {
                // constructor
                RWByteAddressBuffer(_In_ void * buf) : ByteAddressBuffer(buf) {}

                // Store method
                void Store(const uint index, uint value)
                {
                    m_buffer[index/4] = value;
                }
            };

            // Mimic HLSL reinterpreters

            template<typename type>
            inline int asint(type x) 
            {
                return *(reinterpret_cast<int *>(&x));
            }

            template<typename type>
            inline uint asuint(type x) 
            {
                return *(reinterpret_cast<uint *>(&x));
            }

            inline void asuint(double value, uint & lowBits, uint & highBits) 
            {
                uint * p = reinterpret_cast<uint *>(&value);
                lowBits = p[0];
                highBits = p[1];
            }

            template<typename type>
            inline float asfloat(type x) 
            {
                return *(reinterpret_cast<float *>(&x));
            }

            inline double asdouble(uint lowBits, uint highBits) 
            {
                double x;
                uint * p = reinterpret_cast<uint *>(&x);
                p[0] = lowBits;
                p[1] = highBits;
                return x;
            }

            // The barrier implementation, used to mimic the group barrrier.
            // It achieves the group barrier semantics because we luanch a group a time.
            _Barrier * _Barrier_for_kernel;

            // The group barrier that the test DLL uses
            void GroupMemoryBarrierWithGroupSync()
            {
                _Barrier_for_kernel->_Wait();
            }

            void AllMemoryBarrierWithGroupSync()
            {
                _Barrier_for_kernel->_Wait();
            }

            void DeviceMemoryBarrierWithGroupSync()
            {
                _Barrier_for_kernel->_Wait();
            }

            void source_mark() {}

            // Forward declaration
            void __kernel_stub(_In_ void *, uint);
        }  // CPPTestHarness
    } // Test 
} // Concurrency

using namespace Concurrency::Test::CPPTestHarness;

extern "C" {

    // Invoket the kernel
    // The main entry of the test harness
    #pragma warning( disable : 4267 )
    // Disable the warning of "'var' : conversion from 'size_t' to 'type', possible loss of data"
    // These conversion are safe in this function.
    __declspec(dllexport) void cpptest_invoke_kernel(_In_ void *_Handle) 
    {
        _DPC_call_handle * _Handle_ptr = reinterpret_cast<_DPC_call_handle *>(_Handle);

        // count buffers
        size_t _Num_buffers = _Handle_ptr->_M_num_resources;
        size_t _Num_readonly_buffers = 0;
        for (size_t _I = 0; _I < _Num_buffers; ++_I)
        {
            if (_Handle_ptr->_M_device_resource_info[_I]._M_formal_access_mode == _Read_access)
            {
                _Num_readonly_buffers++;
            }
        }
        size_t _Num_readwrite_buffers = _Num_buffers - _Num_readonly_buffers;

        // Copy the buffer from device to host
        void ** _Readonly_buffers = new void*[_Num_readonly_buffers];
        void ** _Readwrite_buffers = new void*[_Num_readwrite_buffers];

        printf("Launching the kernel from CPP test harness\n");

        size_t _Next_readonly_idx = 0, _Next_readwrite_idx = 0;
        for (size_t _I = 0; _I < _Num_buffers; ++_I)
        {
            // Does not support texture
            _ASSERTE(_Handle_ptr->_M_device_resource_info[_I]._Is_buffer());
            // Get access to the master buffer
            _Buffer_descriptor *pBufDesc = _Handle_ptr->_M_device_resource_info[_I]._Get_buffer_desc();
            _Buffer_ptr pSrcBuf;
            _Get_access_async(pBufDesc->_Get_view_key(), pBufDesc->_Get_buffer_ptr()->_Get_master_accelerator_view(), _Read_access, pSrcBuf)._Get();

            char * ptr = new char[pSrcBuf->_Get_num_elems() * pSrcBuf->_Get_elem_size()];
            _Buffer_ptr pDestBuf = _Buffer::_Create_buffer(ptr, accelerator(accelerator::cpu_accelerator).default_view, pSrcBuf->_Get_num_elems(), pSrcBuf->_Get_elem_size());

            _Copy_impl(pSrcBuf, 0, pDestBuf, 0, pSrcBuf->_Get_num_elems())._Get();
            
            if (_Handle_ptr->_M_device_resource_info[_I]._M_formal_access_mode == _Read_access)
            {
                _Readonly_buffers[_Next_readonly_idx++] = reinterpret_cast<void *>(ptr);
            }
            else
            {
                _Readwrite_buffers[_Next_readwrite_idx++] = reinterpret_cast<void *>(ptr);
            }
        }

        // create the group context
        group_context_info _Context;
        _Context.ro_buffers = _Readonly_buffers;
        _Context.rw_buffers = _Readwrite_buffers;

        _Context.const_buffers = new uint*[_Handle_ptr->_M_num_const_buffers];
        for (size_t i = 0; i < _Handle_ptr->_M_num_const_buffers; ++i) {
            _Context.const_buffers[i] = reinterpret_cast<uint*>(_Handle_ptr->_M_const_buffer_info[i]._M_data);
        }

        size_t _Group_size = _Handle_ptr->_M_groupExtentZ * _Handle_ptr->_M_groupExtentY * _Handle_ptr->_M_groupExtentX;
        _Context.gtids = new group_thread_id[_Group_size];

        // compute gtids
        for (size_t _I = 0; _I < _Group_size; _I++) 
        {
            size_t _Flat_idx = _I;
            _Context.gtids[_I].z = _Flat_idx / (_Handle_ptr->_M_groupExtentX * _Handle_ptr->_M_groupExtentY);
            _Flat_idx = _Flat_idx % (_Handle_ptr->_M_groupExtentX * _Handle_ptr->_M_groupExtentY);
            _Context.gtids[_I].y = _Flat_idx / _Handle_ptr->_M_groupExtentX;
            _Flat_idx = _Flat_idx % _Handle_ptr->_M_groupExtentX;
            _Context.gtids[_I].x = _Flat_idx;
        }

        _Context.dtids = new dispatch_thread_id[_Group_size];

        // Simulate the 3D launch
        for (size_t _Z = 0; _Z < _Handle_ptr->_M_groupCountZ; _Z++) 
        {
            for (size_t _Y = 0; _Y < _Handle_ptr->_M_groupCountY; _Y++) 
            {
                for (size_t _X = 0; _X < _Handle_ptr->_M_groupCountX; _X++) 
                {
                    _Context.gid.x = _X;
                    _Context.gid.y = _Y;
                    _Context.gid.z = _Z;
                    for (size_t _I = 0; _I < _Group_size; _I++) 
                    {
                        _Context.dtids[_I].x = _Handle_ptr->_M_groupExtentX * _X + _Context.gtids[_I].x;
                        _Context.dtids[_I].y = _Handle_ptr->_M_groupExtentY * _Y + _Context.gtids[_I].y;
                        _Context.dtids[_I].z = _Handle_ptr->_M_groupExtentZ * _Z + _Context.gtids[_I].z;
                    }

                    _Barrier_for_kernel = new _Barrier(_Group_size);
                    // Launch a group
                    parallel_for(0, (int)_Group_size, [&](int _I)
                    {
                        __kernel_stub(reinterpret_cast<void *>(&_Context), _I);
                    });

                    delete _Barrier_for_kernel;
                }
            }
        }

        // Copy the rw buffer from host to device
        _Next_readonly_idx = 0, _Next_readwrite_idx = 0;
        for (size_t _I = 0; _I < _Num_buffers; ++_I)
        {
            if (_Handle_ptr->_M_device_resource_info[_I]._M_formal_access_mode == _Read_access)
            {
                delete [] _Readonly_buffers[_Next_readonly_idx++];
            }
            else
            {
                // Does not support texture
                _ASSERTE(_Handle_ptr->_M_device_resource_info[_I]._Is_buffer());
                // Get access to the master buffer
                _Buffer_descriptor *pBufDesc = _Handle_ptr->_M_device_resource_info[_I]._Get_buffer_desc();
                _Buffer_ptr pDestBuf;
                _Get_access_async(pBufDesc->_Get_view_key(), pBufDesc->_Get_buffer_ptr()->_Get_master_accelerator_view(), _Write_access, pDestBuf)._Get();

                char * ptr = reinterpret_cast<char *>(_Readwrite_buffers[_Next_readwrite_idx]);
                _Buffer_ptr pSrcBuf = _Buffer::_Create_buffer(ptr, accelerator(accelerator::cpu_accelerator).default_view, pDestBuf->_Get_num_elems(), pDestBuf->_Get_elem_size());

                _Copy_impl(pSrcBuf, 0, pDestBuf, 0, pSrcBuf->_Get_num_elems())._Get();

                delete [] _Readwrite_buffers[_Next_readwrite_idx++];
            }
        }

        delete [] _Readonly_buffers;
        delete [] _Readwrite_buffers;
        delete [] _Context.gtids;
        delete [] _Context.dtids;
        delete [] _Context.const_buffers;
    }
}

#endif // _DEBUG


