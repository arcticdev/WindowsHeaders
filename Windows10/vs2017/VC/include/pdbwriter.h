

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

/* verify that the <rpcsal.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCSAL_H_VERSION__
#define __REQUIRED_RPCSAL_H_VERSION__ 100
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __pdbwriter_h__
#define __pdbwriter_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __PdbSymWriter_FWD_DEFINED__
#define __PdbSymWriter_FWD_DEFINED__

#ifdef __cplusplus
typedef class PdbSymWriter PdbSymWriter;
#else
typedef struct PdbSymWriter PdbSymWriter;
#endif /* __cplusplus */

#endif 	/* __PdbSymWriter_FWD_DEFINED__ */


#ifndef __IPdbWriter_FWD_DEFINED__
#define __IPdbWriter_FWD_DEFINED__
typedef interface IPdbWriter IPdbWriter;

#endif 	/* __IPdbWriter_FWD_DEFINED__ */


#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_pdbwriter_0000_0000 */
/* [local] */ 




extern RPC_IF_HANDLE __MIDL_itf_pdbwriter_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_pdbwriter_0000_0000_v0_0_s_ifspec;


#ifndef __PdbSymLib_LIBRARY_DEFINED__
#define __PdbSymLib_LIBRARY_DEFINED__

/* library PdbSymLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_PdbSymLib;

EXTERN_C const CLSID CLSID_PdbSymWriter;

#ifdef __cplusplus

class DECLSPEC_UUID("520DC67A-752E-11d3-8D56-00C04F680B2B")
PdbSymWriter;
#endif
#endif /* __PdbSymLib_LIBRARY_DEFINED__ */

#ifndef __IPdbWriter_INTERFACE_DEFINED__
#define __IPdbWriter_INTERFACE_DEFINED__

/* interface IPdbWriter */
/* [unique][uuid][object] */ 


EXTERN_C const IID IID_IPdbWriter;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("98ECEE1E-752D-11d3-8D56-00C04F680B2B")
    IPdbWriter : public IUnknown
    {
    public:
        virtual HRESULT STDMETHODCALLTYPE SetPath( 
            /* [in] */ __RPC__in const WCHAR *szFullPathName,
            /* [in] */ __RPC__in_opt IStream *pIStream,
            /* [in] */ BOOL fFullBuild) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE OpenMod( 
            /* [in] */ __RPC__in const WCHAR *szModuleName,
            /* [in] */ __RPC__in const WCHAR *szFileName) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE CloseMod( void) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetPath( 
            /* [in] */ DWORD ccData,
            /* [out] */ __RPC__out DWORD *pccData,
            /* [length_is][size_is][out] */ __RPC__out_ecount_part(ccData, *pccData) WCHAR szPath[  ]) = 0;
        
        virtual HRESULT STDMETHODCALLTYPE GetSignatureAge( 
            /* [out] */ __RPC__out ULONG *sig,
            /* [out] */ __RPC__out ULONG *age) = 0;
        
    };
    
    
#else 	/* C style interface */

    typedef struct IPdbWriterVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            __RPC__in IPdbWriter * This,
            /* [in] */ __RPC__in REFIID riid,
            /* [annotation][iid_is][out] */ 
            _COM_Outptr_  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            __RPC__in IPdbWriter * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            __RPC__in IPdbWriter * This);
        
        HRESULT ( STDMETHODCALLTYPE *SetPath )( 
            __RPC__in IPdbWriter * This,
            /* [in] */ __RPC__in const WCHAR *szFullPathName,
            /* [in] */ __RPC__in_opt IStream *pIStream,
            /* [in] */ BOOL fFullBuild);
        
        HRESULT ( STDMETHODCALLTYPE *OpenMod )( 
            __RPC__in IPdbWriter * This,
            /* [in] */ __RPC__in const WCHAR *szModuleName,
            /* [in] */ __RPC__in const WCHAR *szFileName);
        
        HRESULT ( STDMETHODCALLTYPE *CloseMod )( 
            __RPC__in IPdbWriter * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetPath )( 
            __RPC__in IPdbWriter * This,
            /* [in] */ DWORD ccData,
            /* [out] */ __RPC__out DWORD *pccData,
            /* [length_is][size_is][out] */ __RPC__out_ecount_part(ccData, *pccData) WCHAR szPath[  ]);
        
        HRESULT ( STDMETHODCALLTYPE *GetSignatureAge )( 
            __RPC__in IPdbWriter * This,
            /* [out] */ __RPC__out ULONG *sig,
            /* [out] */ __RPC__out ULONG *age);
        
        END_INTERFACE
    } IPdbWriterVtbl;

    interface IPdbWriter
    {
        CONST_VTBL struct IPdbWriterVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPdbWriter_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IPdbWriter_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IPdbWriter_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IPdbWriter_SetPath(This,szFullPathName,pIStream,fFullBuild)	\
    ( (This)->lpVtbl -> SetPath(This,szFullPathName,pIStream,fFullBuild) ) 

#define IPdbWriter_OpenMod(This,szModuleName,szFileName)	\
    ( (This)->lpVtbl -> OpenMod(This,szModuleName,szFileName) ) 

#define IPdbWriter_CloseMod(This)	\
    ( (This)->lpVtbl -> CloseMod(This) ) 

#define IPdbWriter_GetPath(This,ccData,pccData,szPath)	\
    ( (This)->lpVtbl -> GetPath(This,ccData,pccData,szPath) ) 

#define IPdbWriter_GetSignatureAge(This,sig,age)	\
    ( (This)->lpVtbl -> GetSignatureAge(This,sig,age) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IPdbWriter_INTERFACE_DEFINED__ */


/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


