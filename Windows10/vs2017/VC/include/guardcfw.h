/* Since we're force-included, we should stay
 * out of the way of MIDL and RC */
#if !defined(__midl) && !defined(RC_INVOKED)

#pragma comment(lib, "guardcfw")

/* The CRT includes some .obj files you can link against to set
 * CRT-wide policies, called "linkopts".  Some examples:
 *   nothrownew.obj
 *   smalheap.obj
 * If you are building with cl.exe /guard then you must link against
 * matching linkopts.  To enforce this, we brand each .obj compiled by
 * cl.exe /guard with this string, so they can be checked against the
 * linkopts. */
/* currently only supported when building C++ files */
#ifdef __cplusplus
#ifndef _ALLOW_CL_OPTION_GUARD_MISMATCH
#pragma detect_mismatch("cl_option_/guard", "enabled")
#endif
#endif

/* If wrapping the CRT we link in two Safer C++ runtime initializers.
 * These initializers run before any other CRT initializers.
 * This is mostly used for testing. */
#ifdef _WRAPPED_CRT
#if defined(_ARM_) || defined(_AMD64_)
#pragma comment(linker, "/include:_guard_firstCInit")
#pragma comment(linker, "/include:_guard_firstCPPInit")
#else
#pragma comment(linker, "/include:__guard_firstCInit")
#pragma comment(linker, "/include:__guard_firstCPPInit")
#endif
#endif

#if !defined(WINAPI_FAMILY) || (WINAPI_FAMILY != WINAPI_FAMILY_APP)
#pragma guard(wrap, CoCreateInstance)
#pragma guard(wrap, CoCreateInstanceEx)
#endif

#pragma guard(wrap, CoCreateFreeThreadedMarshaler)
#pragma guard(wrap, CoGetInstanceFromFile)
#pragma guard(wrap, CoGetInstanceFromIStorage)
#pragma guard(wrap, CoQueryClientBlanket)
#pragma guard(wrap, CoTaskMemAlloc)
#pragma guard(wrap, CoTaskMemFree)
#pragma guard(wrap, CoTaskMemRealloc)
#pragma guard(wrap, GetFullPathNameA)
#pragma guard(wrap, GetFullPathNameTransactedA)
#pragma guard(wrap, GetFullPathNameTransactedW)
#pragma guard(wrap, GetFullPathNameW)
#pragma guard(wrap, GetProcAddress)
#pragma guard(wrap, GlobalAlloc)
#pragma guard(wrap, GlobalFree)
#pragma guard(wrap, GlobalLock)
#pragma guard(wrap, GlobalReAlloc)
#pragma guard(wrap, GlobalSize)
#pragma guard(wrap, GlobalUnlock)
#pragma guard(wrap, HeapAlloc)
#pragma guard(wrap, HeapFree)
#pragma guard(wrap, HeapReAlloc)
#pragma guard(wrap, HeapSize)
#pragma guard(wrap, LocalAlloc)
#pragma guard(wrap, LocalFree)
#pragma guard(wrap, LocalReAlloc)
#pragma guard(wrap, LocalSize)
#pragma guard(wrap, MapViewOfFile)
#pragma guard(wrap, MapViewOfFileEx)
#pragma guard(wrap, UnmapViewOfFile)
#pragma guard(wrap, VirtualAlloc)
#pragma guard(wrap, VirtualAllocEx)
#pragma guard(wrap, VirtualAllocExNuma)
#pragma guard(wrap, VirtualFree)
#pragma guard(wrap, VirtualFreeEx)
#pragma guard(wrap, VirtualProtect)
#pragma guard(wrap, VirtualProtectEx)

// CRT ones:
#pragma guard(wrap, _errno)

#pragma guard(wrap, _aligned_offset_malloc)
#pragma guard(wrap, _aligned_offset_malloc_dbg)
// we wrap _aligned_malloc and _aligned_malloc_dbg
// to handle the wrapped (ie non-instrumented) CRT scenario.
#pragma guard(wrap, _aligned_malloc)
#pragma guard(wrap, _aligned_malloc_dbg)

#pragma guard(wrap, _aligned_offset_realloc)
#pragma guard(wrap, _aligned_offset_realloc_dbg)
// wrap _aligned_realloc and _aligned_realloc_dbg
// to handle non-instrumented CRT scenario.
#pragma guard(wrap, _aligned_realloc)
#pragma guard(wrap, _aligned_realloc_dbg)

#pragma guard(wrap, _aligned_offset_recalloc)
#pragma guard(wrap, _aligned_offset_recalloc_dbg)
// wrap _aligned_recalloc and _aligned_recalloc_dbg
// to handle non-instrumented CRT scenario.
#pragma guard(wrap, _aligned_recalloc)
#pragma guard(wrap, _aligned_recalloc_dbg)

#pragma guard(wrap, _aligned_free)
#pragma guard(wrap, _aligned_free_dbg)

#pragma guard(wrap, _aligned_msize)
#pragma guard(wrap, _aligned_msize_dbg)

#pragma guard(wrap, malloc)
#pragma guard(wrap, _malloc_dbg)

#pragma guard(wrap, calloc)
#pragma guard(wrap, _calloc_dbg)

#pragma guard(wrap, realloc)
#pragma guard(wrap, _realloc_dbg)
#pragma guard(wrap, _recalloc)
#pragma guard(wrap, _recalloc_dbg)
#pragma guard(wrap, _expand)
#pragma guard(wrap, _expand_dbg)

#pragma guard(wrap, free)
#pragma guard(wrap, _free_dbg)

// TODO: wrap _free_base and _malloc_base?

#pragma guard(wrap, _msize)
#pragma guard(wrap, _msize_dbg)
#pragma guard(wrap, _CrtSetAllocHook)
#pragma guard(wrap, memcpy)
#pragma guard(wrap, memset)
#pragma guard(wrap, strcat)
#pragma guard(wrap, wcscat)
#pragma guard(wrap, strcpy)
#pragma guard(wrap, wcscpy)
#pragma guard(wrap, _strset)
#pragma guard(wrap, _wcsset)
#pragma guard(wrap, _strdup)

/* winrt __Platform* */
#pragma guard(wrap, __Platform_WindowsCreateString)
#pragma guard(wrap, __Platform_WindowsDeleteString)
#pragma guard(wrap, __Platform_WindowsDuplicateString)
#pragma guard(wrap, __Platform_WindowsGetStringRawBuffer)
#pragma guard(wrap, __Platform_WindowsStringHasEmbeddedNull)
#pragma guard(wrap, __Platform_WindowsCompareStringOrdinal)
#pragma guard(wrap, __Platform_WindowsCreateStringReference)
#pragma guard(wrap, __Platform_WindowsConcatString)
#pragma guard(wrap, __Platform_CoTaskMemAlloc)
#pragma guard(wrap, __Platform_CoTaskMemFree)
#pragma guard(wrap, __Platform_memset)
#pragma guard(wrap, __Platform_CoCreateFreeThreadedMarshaler)

#pragma guard(entry,main)
#pragma guard(entry,wmain)
#pragma guard(entry,WinMain)
#pragma guard(entry,LibMain)
#pragma guard(entry,DllMain)

#pragma guard(entry,DllGetClassObject)
#pragma guard(entry,DllGetActivationFactory)

#endif /* !defined(__midl) && !defined(RC_INVOKED) */
