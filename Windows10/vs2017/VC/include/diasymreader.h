

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 8.00.0603 */
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

/* verify that the <rpcsal.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCSAL_H_VERSION__
#define __REQUIRED_RPCSAL_H_VERSION__ 100
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__


#ifndef __diasymreader_h__
#define __diasymreader_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __PdbSymWriter_FWD_DEFINED__
#define __PdbSymWriter_FWD_DEFINED__

#ifdef __cplusplus
typedef class PdbSymWriter PdbSymWriter;
#else
typedef struct PdbSymWriter PdbSymWriter;
#endif /* __cplusplus */

#endif 	/* __PdbSymWriter_FWD_DEFINED__ */


#ifndef __PdbSymReader_FWD_DEFINED__
#define __PdbSymReader_FWD_DEFINED__

#ifdef __cplusplus
typedef class PdbSymReader PdbSymReader;
#else
typedef struct PdbSymReader PdbSymReader;
#endif /* __cplusplus */

#endif 	/* __PdbSymReader_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
#include "corsym.h"

#ifdef __cplusplus
extern "C"{
#endif 



#ifndef __CorSymLib_LIBRARY_DEFINED__
#define __CorSymLib_LIBRARY_DEFINED__

/* library CorSymLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_CorSymLib;

EXTERN_C const CLSID CLSID_PdbSymWriter;

#ifdef __cplusplus

class DECLSPEC_UUID("85653ECA-CC30-4f04-8318-6E293FC4E11F")
PdbSymWriter;
#endif

EXTERN_C const CLSID CLSID_PdbSymReader;

#ifdef __cplusplus

class DECLSPEC_UUID("29C98DFC-AC6B-4788-BDDD-CA41D6D3704A")
PdbSymReader;
#endif
#endif /* __CorSymLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


