// pogomgrapi.h
//
// Package up PGOMgr functionality as a dll that can be consumed
// by external clients

// MergePGC
//
// wzPGCPath: Location and name of the PGC file
// wzPGDPath: Location and name of the PGD file
extern "C" __declspec(dllimport) bool MergePGC(LPWSTR wzPGCPath, LPWSTR wzPGDPath);

// SetPgoLibOptions
//      Sets flags that affect the behaviour of other functions exported by the DLL
// fUniqueFuncNames: The names returned by the GetFunctionStats function will be unique(undecorated function names)
extern "C" __declspec(dllimport) bool SetPgoLibOptions(bool fUniqueFuncNames);

// OpenStatsHandle
//      Opens the file specified for the purpose of calling GetHeaderStats and GetFunctionStats
// wzPGDPath: Location and name of the PGD file
extern "C" __declspec(dllimport) bool OpenStatsHandle(LPWSTR wzPGDPath);

// CloseStatsHandle
//      Closes a file previously opened by OpenStatsHandle
extern "C" __declspec(dllimport) void CloseStatsHandle();

// GetHeaderStats
//
//      Returns information about a binary. All parameters are out parameters.
// pModuleCount: Number of modules in this binary
// pFunctionCount: Total number of functions in this binary.
// pArcCount: Number of arcs (simple probes)
// pValueCount: Number of value probes
// pStaticCount: Total static instruction count 
// pBasicBlocks: Total number of basic blocks
// pAvgBBSize: Average basic block size
// pDynSize: Total dynamic instruction count
extern "C" __declspec(dllimport) bool GetHeaderStats(QWORD* pModuleCount, QWORD* pFunctionCount, QWORD* pArcCount, QWORD* pValueCount, QWORD* pStaticCount, QWORD* pBasicBlocks, double* pAvgBBSize, QWORD* pDynSize);

// GetFunctionStats
//
//      Returns information about a specific function. All params except func are out parameters.
// func: A zero based index into a list of functions sorted by dynmic instruction count 
//      descending. Max value is FunctionCount-1 (returned from GetHeaderStats).
// pName: Buffer to store name in.
// pNameLen: Length of pName, in characters
// pEntryCount: Number of times this function was invoked in the profile
// pStaticCount: Size of this function
// pDynCount: Dynamic instruction count (total count of instructions executed in this function)
// pTotal: Total percentage of dynamic instruction count in this profile from this function
// pRunTotal: Percentile this function falls in
extern "C" __declspec(dllimport) bool GetFunctionStats(QWORD func, LPWSTR pName, ULONG pNameLen, QWORD* pEntryCount, QWORD* pStaticCount, QWORD* pDynCount, double* pTotal, double* pRunTotal);

// GetNextInputFile
//
//      Returns information about the next input file to be enumerated in the currently opened database.
// reset: Flag specifying whether to start enumerating files from the beginning.
// pName: Buffer to store name in.
// pNameLen: Length of pName, in characters.
extern "C" __declspec(dllimport) bool GetNextInputFile(bool reset, LPWSTR pName, ULONG pNameLen);

// GetFunctionNextBlockInfo
//
//      Returns information about a block belonging to a certain function.
// func: A zero based index into a list of functions sorted by dynmic instruction count 
//      descending. Max value is FunctionCount-1 (returned from GetHeaderStats).
// reset: Flag specifying whether to start enumerating blocks from the beginning.
// unLineStart: Start line number of the block
// unLineEnd: End line number of the block
// unDynInstructions: Number of dynamic instructions in the block.
// unHitCount: Number of times the block was hit.
// unNumInstr: NUmber of instructions in the block.
extern "C" __declspec(dllimport) bool GetFunctionNextBlockInfo(QWORD func, bool reset, ULONG *unLineStart, ULONG *unLineEnd, QWORD *unDynInstructions, QWORD *unHitCount, ULONG *unNumInstr);

